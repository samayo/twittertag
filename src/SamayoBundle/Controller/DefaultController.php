<?php

namespace SamayoBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Abraham\TwitterOAuth\TwitterOAuth;
use Symfony\Component\HttpFoundation\Session;
use SamayoBundle\Entity\Users;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('HashtagBundle:default:index.html.twig', [
            'welcome' => 'Welcome to symfony app'
        ]);
    }


    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function showHashtagsAction(Request $request)
    {
        $twitter = $this->get('endroid.twitter');
        $result = $twitter->query("search/tweets", "GET", "json", ['count' => 5, 'q' => '#symfony']);
        $tweets = json_decode($result->getContent());
        return $this->render("HashtagBundle:default:show.html.twig", array(
            'tweets' => $tweets
        ));
    }


    /**
     * @Route("/twitter-callback", name="twitter-callback")
     */
    public function twitterCallbackAction(Request $request)
    {
        $session = $request->getSession();

        $oauth_token = $request->get('oauth_token');
        $oauth_verifier = $request->get('oauth_verifier');
        $oauth_token_secret = $session->get('oauth_token_secret');


        if(!$oauth_token || !$oauth_verifier && !$oauth_token_secret){
            throw new \Exception("Invalid auth handshake");
        }

        $user = new Users;
        $user->setOauthToken($oauth_token);
        $user->setOauthTokenSecret('oauth_token_secret');
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        if(!$user->getId()){
            throw new \Exception("User not saved in database, unknown error occured, please try again");
        }

        if($oauth_verifier && $oauth_token && $oauth_token_secret){
            return $this->redirectToRoute('dashboard');
        }
    }

    /**
     * @Route("/twitter-login", name="twitter-login")
     */
    public function twitterLoginAction(Request $request)
    {

        $consumer_key = $this->getParameter('consumer_key');
        $consumer_secret = $this->getParameter('consumer_secret');
        $access_token = $this->getParameter('access_token');
        $access_token_secret = $this->getParameter('access_token_secret');

        $twitter_oauth = new TwitterOAuth(
            $consumer_key,
            $consumer_secret
        );

        $twitter_oauth->setTimeouts(10, 15);

        $request_token = $twitter_oauth->oauth(
            'oauth/request_token', [
                'oauth_callback' => 'http://localhost.dev/twitter-callback'
            ]
        );

        if($twitter_oauth->getLastHttpCode() != 200){
            throw new \Exception('There was a problem with your request');
        }

        $url = $twitter_oauth->url(
            'oauth/authorize', [
                'oauth_token' => $request_token['oauth_token']
            ]
        );
        return $this->redirect($url);
    }

    /**
     * @Route("/ajax", name="change_hashtag")
     */
    public function changeHashtagAction(Request $request){
        if($request->isXmlHttpRequest()){

            $hashtag = $request->get('keyword');


            $twitter = $this->get('endroid.twitter');
            $result = $twitter->query("search/tweets", "GET", "json", ['count' => 5, 'q' => $hashtag]);
            $tweets = $result->getContent();

            return new JsonResponse(['response' => $tweets]);
        }

        return new JsonResponse(["response" => "ERROR! This is not a valid XHR Request"]);
    }

}
