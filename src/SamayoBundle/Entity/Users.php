<?php

namespace SamayoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Users
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="SamayoBundle\Entity\UsersRepository")
 */
class Users
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="oauth_token", type="string", length=255)
     */
    private $oauthToken;

    /**
     * @var string
     *
     * @ORM\Column(name="oauth_token_secret", type="string", length=255)
     */
    private $oauthTokenSecret;

    /**
     * @var string
     *
     * @ORM\Column(name="oauth_provider", type="string", length=255)
     */
    private $oauthProvider;

    /**
     * @var string
     *
     * @ORM\Column(name="oauth_uid", type="string", length=255)
     */
    private $oauthUid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set oauthToken
     *
     * @param string $oauthToken
     * @return Users
     */
    public function setOauthToken($oauthToken)
    {
        $this->oauthToken = $oauthToken;

        return $this;
    }

    /**
     * Get oauthToken
     *
     * @return string 
     */
    public function getOauthToken()
    {
        return $this->oauthToken;
    }

    /**
     * Set oauthTokenSecret
     *
     * @param string $oauthTokenSecret
     * @return Users
     */
    public function setOauthTokenSecret($oauthTokenSecret)
    {
        $this->oauthTokenSecret = $oauthTokenSecret;

        return $this;
    }

    /**
     * Get oauthTokenSecret
     *
     * @return string 
     */
    public function getOauthTokenSecret()
    {
        return $this->oauthTokenSecret;
    }

    /**
     * Set oauthProvider
     *
     * @param string $oauthProvider
     * @return Users
     */
    public function setOauthProvider($oauthProvider)
    {
        $this->oauthProvider = $oauthProvider;

        return $this;
    }

    /**
     * Get oauthProvider
     *
     * @return string 
     */
    public function getOauthProvider()
    {
        return $this->oauthProvider;
    }

    /**
     * Set oauthUid
     *
     * @param string $oauthUid
     * @return Users
     */
    public function setOauthUid($oauthUid)
    {
        $this->oauthUid = $oauthUid;

        return $this;
    }

    /**
     * Get oauthUid
     *
     * @return string 
     */
    public function getOauthUid()
    {
        return $this->oauthUid;
    }
}
